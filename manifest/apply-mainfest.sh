#!/bin/bash

#Apply Mainfest
kubectl apply -f secret.yaml
kubectl apply -f mysql-service.yaml
kubectl apply -f mysql-pv.yaml
kubectl apply -f mysql-pvc.yaml
kubectl apply -f mysql.yaml
sleep 5
kubectl apply -f app-service.yaml
kubectl apply -f app-deploymnet.yaml

# check pods 
kubectl get deploy 
kubectl get pods 
watch kubectl get pods 

