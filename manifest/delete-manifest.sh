#!/bin/bash

kubectl  delete -f db-secrets.yaml
kubectl  delete -f mysql-service.yaml
kubectl  delete -f mysql-pv.yaml &
kubectl  delete -f mysql-pvc.yaml &
kubectl  delete -f mysql.yaml
kubectl  delete -f app-service.yaml
kubectl  delete -f app-deploymnet.yaml
kubectl delete deploy --all &
sleep 2
kubectl delete secrets --all &
sleep 2
kubectl delete svc --all &
kubectl delete pv --all &
kubectl delete pvc --all &

#check pods 
watch kubectl get pods
