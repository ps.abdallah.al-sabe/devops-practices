#!/bin/bash
export  name="training"
export  namespace="devops"
export  releaseName="${namespace}-${name}"
export  default_helm_wait="900"
export  HELM_ARGS="--install
                   --atomic
                   --force
		   --wait
                   --timeout ${default_helm_wait}
                   --namespace=${namespace}"
		   helm upgrade ${HELM_ARGS} ${releaseName} --set global.app.image.tag=$IMAGETAG .
