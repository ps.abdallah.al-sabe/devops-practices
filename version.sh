#!/bin/bash


export DATEFORMAT=$(git log -n 1 --format='%cd' --date=format:'%y%m%d')
export COMMIT=$(git rev-parse --short=8 HEAD)
export version=${DATEFORMAT}-${COMMIT}
echo $version
