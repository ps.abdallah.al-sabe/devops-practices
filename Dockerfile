FROM openjdk:8
COPY target/assignment*.jar /usr/app/
WORKDIR /usr/app
ENV SPRING_PROFILE_ACTIVE=mysql
EXPOSE 8090
ENTRYPOINT java -jar -Dspring.profiles.active=${SPRING_PROFILE_ACTIVE} assignment*.jar